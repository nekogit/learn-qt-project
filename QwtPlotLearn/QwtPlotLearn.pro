QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += QT_DLL QWT_DLL
LIBS += -L"D:\Qt\5.12.8\5.12.8\msvc2015_64\lib" -lqwtd
LIBS += -L"D:\Qt\5.12.8\5.12.8\msvc2015_64\lib" -lqwt
INCLUDEPATH += D:\Qt\5.12.8\5.12.8\msvc2015_64\include\Qwt

CONFIG += qwt

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    dialog.cpp

HEADERS += \
    dialog.h

FORMS += \
    dialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

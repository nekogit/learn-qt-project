#include "dialog.h"
#include "ui_dialog.h"
#include <QFont>
#include <QBrush>
#include <qwt_text.h>
#include <qwt_plot.h>
#include <qwt_series_data.h>
#include <qwt_plot_curve.h>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    int width = this->width() - 10;
    int height = this->height() - 10;
    ui->qwtPlot->move(0,0);
    ui->qwtPlot->resize(width,height);

    ui->qwtPlot->setTitle(QString::fromLocal8Bit("QwtPlot Test"));

    QwtText t;
    t.setText(QString::fromLocal8Bit("QwlPlot Test For QwtText"));

    QFont font;

    font.setBold(true);
    font.setItalic(true);

    t.setFont(font);
    t.setColor(QColor(255,0,0));
    QBrush brush(QColor(0,0,255));
    t.setBackgroundBrush(brush);

    ui->qwtPlot->setTitle(t);

    ui->qwtPlot->setAxisScale(QwtPlot::xBottom,0,100,10);
    ui->qwtPlot->setAxisScale(QwtPlot::xTop,0,100,5);
    ui->qwtPlot->setAxisScale(QwtPlot::yLeft,0,200,5);
    ui->qwtPlot->setAxisScale(QwtPlot::yRight,0,200,5);

    ui->qwtPlot->enableAxis(QwtPlot::xTop,true);
    ui->qwtPlot->enableAxis(QwtPlot::yRight,true);

    ui->qwtPlot->setAxisTitle(QwtPlot::xBottom,QString::fromLocal8Bit("bottom"));
    ui->qwtPlot->setAxisTitle(QwtPlot::yLeft,QString::fromLocal8Bit("Left"));


    float M_PI = 3.1415926f;
    //数据x,y值保存
    QVector<QPointF> vector;
    for(int i =0;i<100;i++){
        QPointF point;
        point.setX(i);
        int y = 20*sin(i*M_PI/10) + 50;
        point.setY(y);
        vector.append(point);
    }
    //构造曲线数据
    QwtPointSeriesData* series = new QwtPointSeriesData(vector);

    //create plot item
    QwtPlotCurve* curve1 = new QwtPlotCurve("Curve 1");
    //设置数据
    curve1->setData(series);
    //把曲线附加到qwtPlot上
    curve1->attach(ui->qwtPlot);

    ui->qwtPlot->replot();
    ui->qwtPlot->show();

}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::do_test()
{

}

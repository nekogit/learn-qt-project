﻿#ifndef TREEMODEL_H
#define TREEMODEL_H

#endif // TREEMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include "TreeItem.h"

class TreeModel :public QAbstractItemModel
{
    Q_OBJECT

public:
    TreeModel(const QStringList &headers,const QString &data,QObject *parent = nullptr);
    ~TreeModel();






    // QAbstractItemModel interface neccessary
public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;
    QModelIndex parent(const QModelIndex &child) const override;
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;




    // QAbstractItemModel interface unneccessary
public:
//    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
//    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
//    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role) override;
//    bool insertRows(int row, int count, const QModelIndex &parent) override;
//    bool insertColumns(int column, int count, const QModelIndex &parent) override;
//    bool removeRows(int row, int count, const QModelIndex &parent) override;
//    bool removeColumns(int column, int count, const QModelIndex &parent) override;

private:
//    void setupModelData(const QStringList &lines,TreeItem * parent);
    TreeItem * getItem(const QModelIndex &index) const;

    TreeItem * rootItem;
};

TreeModel::TreeModel(const QStringList &headers, const QString &data, QObject *parent)
    :QAbstractItemModel(parent)
{
    QVector<QVariant> rootData;
    for(const QString &header : headers)
        rootData << header;

    rootItem = new TreeItem(rootData);
    setupModelData(data.split('\n'),rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

TreeItem * TreeModel::getItem(const QModelIndex &index) const
{
    if(index.isValid()){
        TreeItem *item = static_cast<TreeItem *>(index.internalPointer());
        if(item)
            return item;
    }
    return rootItem;
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    const TreeItem *parentItem = getItem(parent);

    return parentItem ? parentItem->childCount() : 0;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return rootItem->columnCount();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if(!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable | QAbstractItemModel::flags(index);
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{
    if(parent.isValid() && parent.column() != 0)
        return QModelIndex();
    TreeItem * parentItem = getItem(parent);
    if(!parentItem)
        return QModelIndex();

    TreeItem * childItem = parentItem->child(row);
    if(childItem)
    {
        return createIndex(row,column,childItem);
    }
    return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &child) const
{
    if(!child.isValid())
        return QModelIndex();

    TreeItem * childItem = getItem(child);
    TreeItem * parentItem = childItem ? childItem->parent() : nullptr;

    if(parentItem == rootItem || !parentItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(),0,parentItem);
}


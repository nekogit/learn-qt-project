﻿#ifndef TREEHEADERSTRUCT_H
#define TREEHEADERSTRUCT_H

#endif // TREEHEADERSTRUCT_H


typedef struct Person_t{
    QString name;
    QString sex;
    int age;
    QString phone;
    Person_t()
    {
        age = 0;
    }
} Person;


typedef struct Province_t{
    QString name;
    QVector<Person *>people;
} Province;

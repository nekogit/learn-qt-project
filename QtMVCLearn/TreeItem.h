﻿#ifndef TREEITEM_H
#define TREEITEM_H

#endif // TREEITEM_H

#include <QObject>
#include <QVariant>
#include <QVector>


class TreeItem{

public:
    explicit TreeItem(const QVector<QVariant> &data,TreeItem * parent = nullptr);
    ~TreeItem();

    TreeItem * child(int number);   //返回指定的孩子节点
    int childCount() const; //返回孩子数
    int columnCount() const;    //返回本节点数据列数
    QVariant data(int column) const;   //返回指定数据列数的数据
    bool insertChildren(int position,int count,int columns);    //在指定位置插入指定数目的指定数据列
    bool insertColumns(int position,int columns);   //在指定位置插入指定数目的数据列
    TreeItem * parent();    //返回该节点的父节点
    bool removeChildren(int position,int count);    //移除指定位置的count个孩子节点
    bool removeColumns(int position,int columns);   //移除指定位置的columns列节点数据
    int childNumber() const;    //返回该孩子节点的节点序号
    bool setData(int columns,const QVariant &value);    //设置该孩子节点的指定列数的数据

private:
    QVector<TreeItem *> childItems; //该节点的孩子节点
    QVector<QVariant> itemData; //该节点的数据集合
    TreeItem *parentItem;   //该节点的父节点
};

TreeItem::TreeItem(const QVector<QVariant> &data, TreeItem *parent):itemData(data),parentItem(parent)
{

}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
}

TreeItem *TreeItem::parent()
{
    return parentItem;
}

TreeItem *TreeItem::child(int number)
{
    if(number < 0 || number >= childItems.size())
        return nullptr;
    return childItems.at(number);
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::childNumber() const
{
    if(parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));
    return 0;
}

int TreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeItem::data(int column) const
{
    if(column < 0 || column >= itemData.size())
        return QVariant();
    return itemData.at(column);
}

bool TreeItem::setData(int columns, const QVariant &value)
{
    if(columns < 0 || columns >= itemData.size())
        return false;

    itemData[columns] = value;
    return true;
}

bool TreeItem::insertChildren(int position, int count, int columns)
{
    if(position < 0 || position > childItems.size())
        return false;

    for(int row = 0; row < count; ++row)
    {
        QVector<QVariant> data(columns);
        TreeItem *item = new TreeItem(data,this);
        childItems.insert(position,item);
    }

    return true;
}

bool TreeItem::removeChildren(int position, int count)
{
    if(position < 0 || position >= childItems.size())
        return false;

    for(int row = 0; row < count ; ++row)
    {
        delete childItems.takeAt(position);
    }

    return true;
}

bool TreeItem::insertColumns(int position, int columns)
{
    if(position < 0 || position >= itemData.size())
        return false;

    for(int column = 0 ; column < columns ; ++column)
    {
        itemData.insert(position,QVariant());
    }

    for(TreeItem *child: qAsConst(childItems))
        child->insertColumns(position,columns);

    return true;
}

bool TreeItem::removeColumns(int position, int columns)
{
    if(position < 0 || position >= itemData.size())
        return false;

    for(int column = 0 ;column < columns ; ++column)
    {
        delete itemData.takeAt(position);
    }

    return true;
}




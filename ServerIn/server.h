#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QString>
#include <QDebug>
#include <QtNetwork>
#include <QNetworkInterface>
#include <QRandomGenerator>
class Server : public QObject
{
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

private slots:
    void sendData();

private:
    void initServer();

    QTcpServer *server = nullptr;

};

#endif // SERVER_H

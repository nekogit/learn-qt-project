#include "server.h"

Server::Server(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(this);
    connect(server,&QTcpServer::newConnection,this,&Server::sendData);


    if(!server->listen(QHostAddress::AnyIPv4,11095)){   //设置监听的地址为所有ipv4,及指定端口
        qDebug()<<"Server: Unable to start the server: "<<server->errorString();

        return;
    }
    QString ipAddress;
    const QList<QHostAddress> ipAddressList = QNetworkInterface::allAddresses();
    for(const QHostAddress &entry : ipAddressList)
    {
        if(entry != QHostAddress::LocalHost && entry.toIPv4Address())
        {
            ipAddress = entry.toString();   //获取本地的地址
            break;
        }
    }

    if(ipAddress.isEmpty()) //上述获取方式失败,重新选定获取的地址
        ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
    qDebug()<<tr("the server is running at:")<<ipAddress<<", "<<server->serverPort();


}

void Server::sendData()
{
    QByteArray block;
    QDataStream stream(&block,QIODevice::WriteOnly);    //对比client端,流设备的另一种初始化方式
    stream.setVersion(QDataStream::Qt_5_12);
    QString data = QString::number(QRandomGenerator::global()->bounded(0,1000));
    stream << data;
    qDebug()<<"Server send data: "<<data;

    QTcpSocket *socket = server->nextPendingConnection();   //接受请求的socket连接并返回socket指针,提供操作权
    connect(socket,&QAbstractSocket::disconnected,socket,&QObject::deleteLater);

    socket->write(block);
    qDebug()<<socket->peerAddress()<<QString::number(socket->peerPort());
    socket->disconnectFromHost();   //server端使socket主动断开连接
}

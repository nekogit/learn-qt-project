﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#define LOG qDebug()<<__FILE__<<__LINE__<<QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss:zzz")

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    //ui->InputTextEdit->clear();
    ui->OutputTextEdit->clear();
    QProcess *process = new QProcess(this);
    process->setProgram("cmd"); //use system program as example
    QStringList argument;
    argument<<"/c"<<ui->InputTextEdit->toPlainText();   //way to use argument ,load by function.
    process->setArguments(argument);
    process->start();

    //process->waitForStarted();
    process->waitForFinished();

    //ui->OutputTextEdit->setText(QString::fromLocal8Bit(process->readAllStandardOutput()));
    ui->OutputTextEdit->setText(QString::fromLocal8Bit(process->readAll()));
    //ui->OutputTextEdit->setText(QString::number(process->exitStatus()));
    LOG << u8"finished";
}

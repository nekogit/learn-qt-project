﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QTreeView *treeView = ui->treeView;

    TreeModel *model = new TreeModel(treeView);

    treeView->setEditTriggers(QTreeView::NoEditTriggers);
    treeView->setSelectionBehavior(QTreeView::SelectRows);
    treeView->setSelectionMode(QTreeView::SingleSelection);
    treeView->setFocusPolicy(Qt::NoFocus);

    treeView->setModel(model);
}

MainWindow::~MainWindow()
{
    delete ui;
}


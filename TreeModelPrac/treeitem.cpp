﻿#include "treeitem.h"

TreeItem::TreeItem(QList<QVariant> data,TreeItem *parentItem):mParentItem(parentItem)
  ,mItemData(data)
{
    mItemData = data;
}

TreeItem::~TreeItem()
{
    qDeleteAll(mChildItems);
}

TreeItem * TreeItem::child(int row)
{
    if(row < 0 || row >= mChildItems.size())
        return nullptr;
    return mChildItems.value(row);
}

TreeItem * TreeItem::parent()
{
    return mParentItem;
}

int TreeItem::childCount() const
{
    return mChildItems.size();
}

int TreeItem::columnCount() const
{
    //int Msize = mItemData.size();
    return mItemData.size();
}

int TreeItem::row() const
{
    if(mParentItem)
        return mParentItem->mChildItems.indexOf(const_cast<TreeItem*>(this));
    else
        return 0;
}

QVariant TreeItem::data(int column) const
{
    if(column < 0 || column >= mItemData.size())
        return QVariant();
    else
        return mItemData.value(column);
}

void TreeItem::appendChild(TreeItem *child)
{
    mChildItems.append(child);
}

void TreeItem::removeChilds()
{
    for(int index = 0;index < mChildItems.size(); index++)
    {
        mChildItems[index]->removeChilds();
    }
    qDeleteAll(mChildItems);
    mChildItems.clear();
}

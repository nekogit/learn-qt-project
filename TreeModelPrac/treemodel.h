﻿#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QObject>
#include <QAbstractItemModel>
#include <QModelIndex>
#include "treeitem.h"

class TreeModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit TreeModel(QObject * parent = 0);
    ~TreeModel();



    // QAbstractItemModel interface
public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const override;//返回给定parent节点下的(row,column)子节点索引.
    QModelIndex parent(const QModelIndex &child) const override;//返回指定索引的item的父节点索引
    int rowCount(const QModelIndex &parent) const override;//返回指定父节点的子节点数目
    int columnCount(const QModelIndex &parent) const override;//返回指定父节点的数据列数
    QVariant data(const QModelIndex &index, int role) const override;//核心函数,对给定的index访问对应的role值,由role值决定访问index对应item的哪个存储值.

private:
    TreeItem* mRootItem;
    QModelIndex mCurrentIndex;

};

#endif // TREEMODEL_H

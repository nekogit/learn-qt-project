﻿#include "treemodel.h"

TreeModel::TreeModel(QObject *parent)
{
    mRootItem = new TreeItem({"root"},nullptr);
    TreeItem * item1 = new TreeItem({"testItem"},mRootItem);
    TreeItem * item2 = new TreeItem({"testChild"},item1);
    item1->appendChild(item2);
    mRootItem->appendChild(item1);
}

TreeModel::~TreeModel()
{
    if(NULL != mRootItem)
        delete mRootItem;
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const
{

    if(!hasIndex(row,column,parent))
        return QModelIndex();

    TreeItem *parentItem;
    if(!parent.isValid())
        parentItem = mRootItem;
    else
    parentItem = static_cast<TreeItem *>(parent.internalPointer());

    TreeItem *TargetItem = parentItem->child(row);

    if(TargetItem)
        return createIndex(row,column,TargetItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &child) const
{
    if(!child.isValid())
        return QModelIndex();

    TreeItem * childItem = static_cast<TreeItem *>(child.internalPointer());
    TreeItem * parentItem = childItem->parent();
    if(parentItem != mRootItem)
        return createIndex(parentItem->row(),0,parentItem);
    else
        return QModelIndex();
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem * parentItem;
    if(!parent.isValid())
        parentItem = mRootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    return 2;

    if(!parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return mRootItem->columnCount();

}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    if(!index.isValid())
        return QVariant();

    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    if(role == Qt::DisplayRole)
    {
        return item->data(index.column());
    }
    else return QVariant();
}

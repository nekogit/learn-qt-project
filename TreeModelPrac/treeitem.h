﻿#ifndef TREEITEM_H
#define TREEITEM_H

#include <QObject>
#include <QVariant>
#include <QList>

class TreeItem
{
public:
    explicit TreeItem(QList<QVariant> data,TreeItem *parentItem = 0);
    ~TreeItem();

    TreeItem *child(int row);
    TreeItem *parent();

    int childCount() const;
    int columnCount() const;
    int row() const;

    QVariant data(int column) const;

    int level(){return mLevel;}
    void setLevel(int level){ mLevel = level;}

    void setData(QList<QVariant> data){mItemData = data;}

    void setRow(int row){mRow = row;}

    void appendChild(TreeItem *child);
    void removeChilds();


private:
    QList<TreeItem *> mChildItems;
    TreeItem *mParentItem;
    QList<QVariant> mItemData;
    int mLevel;
//    void *mPtr;
    int mRow;

};

#endif // TREEITEM_H

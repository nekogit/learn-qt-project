/********************************************************************************
** Form generated from reading UI file 'dialog.ui'
**
** Created by: Qt User Interface Compiler version 6.2.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOG_H
#define UI_DIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QProgressBar *progressBar;
    QPushButton *pB_start;
    QPushButton *pB_Pause;
    QPushButton *pB_Resume;
    QPushButton *pB_Stop;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QString::fromUtf8("Dialog"));
        Dialog->resize(800, 600);
        progressBar = new QProgressBar(Dialog);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setGeometry(QRect(210, 240, 401, 61));
        progressBar->setValue(0);
        pB_start = new QPushButton(Dialog);
        pB_start->setObjectName(QString::fromUtf8("pB_start"));
        pB_start->setGeometry(QRect(20, 460, 131, 51));
        pB_Pause = new QPushButton(Dialog);
        pB_Pause->setObjectName(QString::fromUtf8("pB_Pause"));
        pB_Pause->setGeometry(QRect(200, 460, 131, 51));
        pB_Resume = new QPushButton(Dialog);
        pB_Resume->setObjectName(QString::fromUtf8("pB_Resume"));
        pB_Resume->setGeometry(QRect(390, 460, 131, 51));
        pB_Stop = new QPushButton(Dialog);
        pB_Stop->setObjectName(QString::fromUtf8("pB_Stop"));
        pB_Stop->setGeometry(QRect(580, 460, 141, 51));

        retranslateUi(Dialog);

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QCoreApplication::translate("Dialog", "Dialog", nullptr));
        progressBar->setFormat(QCoreApplication::translate("Dialog", "%p%", nullptr));
        pB_start->setText(QCoreApplication::translate("Dialog", "START", nullptr));
        pB_Pause->setText(QCoreApplication::translate("Dialog", "PAUSE", nullptr));
        pB_Resume->setText(QCoreApplication::translate("Dialog", "RESUME", nullptr));
        pB_Stop->setText(QCoreApplication::translate("Dialog", "STOP", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOG_H

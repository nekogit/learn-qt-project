﻿#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QFuture>
#include <QPromise>
#include <QThread>
#include <QtConcurrent>
#include <QDebug>

#include "ProgressListener.h"



QT_BEGIN_NAMESPACE
namespace Ui {
class Dialog;
}
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:



    Dialog(QWidget *parent = nullptr);
    ~Dialog();

    void countProgress(QPromise<int> &promise);

Q_SIGNALS:
    void setRunningState(bool b);
    void setCountingState(bool b);

private slots:
    void on_pB_start_clicked();

    void on_pB_Pause_clicked();

    void on_pB_Resume_clicked();

    void on_value_set(int);

    void on_pB_Stop_clicked();

private:

    Ui::Dialog *ui;

    Listener *listener;

    QFuture<int> mFuture;




};
#endif // DIALOG_H

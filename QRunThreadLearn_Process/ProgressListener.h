#ifndef PROGRESSLISTENER_H
#define PROGRESSLISTENER_H

#include <QObject>
#include <QPromise>
#include <QtConcurrent>
#include <QThread>
#include <QFuture>


class Listener : public QThread
{
    Q_OBJECT

public:
    explicit Listener(){}
    ~Listener(){}

    // void countNum(QPromise<int> &promise){
    //     for(int i = 0; i < 100 ; i++)
    //     {
    //         promise.suspendIfRequested();

    //         if(promise.isCanceled())
    //         {
    //             return ;
    //         }
    //         qDebug()<<QString::number(i);
    //         emit bar_value_set(i);
    //         promise.addResult(i);
    //         QThread::sleep(1);
    //     }
    // }

    void countNum(){
        int i = 0;
        while(i < 101)
        {
            if(100 == i)
            {
                i = 0;
                continue;
            }
            else
            {
                qDebug()<<"target = "<<QString::number(i);
                emit bar_value_set(i);
                QThread::sleep(1);
                i++;
            }
        }
    }



Q_SIGNALS:
    void bar_value_set(int);

private slots:
    void setRunningState(bool b){
        isRunning = b;
    }

    void setCountingState(bool b){
        isCounting = b;
    }

private:
    bool isRunning = true;
    bool isCounting = true;

    // QThread interface
protected:
    void run() override{
        int i = 0;
        while(i < 101)
        {
            if(isRunning)
            {

            }
            else
                return ;

            if(isCounting)
            {
                if(100 == i)
                {
                    i = 0;
                    continue;
                }
                else
                {
                    qDebug()<<"target = "<<QString::number(i)<<" CurrentThread: "<<QThread::currentThread();
                    emit bar_value_set(i);
                    QThread::sleep(1);
                    i++;
                }
            }
            else
                continue;
        }
    }
};


#endif // PROGRESSLISTENER_H

﻿#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    // listener = new Listener();

    // //listener->moveToThread(thread);
    // connect(listener,SIGNAL(bar_value_set(int)),this,SLOT(on_value_set(int)),Qt::QueuedConnection);
    // connect(this,SIGNAL(setRunningState(bool)),listener,SLOT(setRunningState(bool)));
    // connect(this,SIGNAL(setCountingState(bool)),listener,SLOT(setCountingState(bool)));
}

Dialog::~Dialog()
{
    listener->deleteLater();
    //mFuture.waitForFinished();
    delete ui;
}

// void Dialog::countProgress(QPromise<int> &promise)
// {

//     for(int i = 0; i < 100 ; i++)
//     {
//         promise.suspendIfRequested();

//         if(promise.isCanceled())
//         {
//             return ;
//         }

//         QThread::sleep(1);
//         ui->progressBar->setValue(i);
//         promise.addResult(i);
//     }
// }

extern void count(QPromise<int> &promise)
{
    for(int i = 0; i < 100 ; i++)
    {
        promise.suspendIfRequested();

        if(promise.isCanceled())
        {
            return ;
        }

        QThread::sleep(1);
        qDebug()<<QString::number(i);
        promise.addResult(i);
    }
}

void Dialog::on_pB_start_clicked()
{
    //mFuture = QtConcurrent::run(QThreadPool::globalInstance(),countProgress);
    //mFuture = QtConcurrent::run(this,&Dialog::countProgress);
    //mFuture = QtConcurrent::run(count);

    listener = new Listener();

    //listener->moveToThread(thread);
    connect(listener,SIGNAL(bar_value_set(int)),this,SLOT(on_value_set(int)),Qt::QueuedConnection);
    connect(this,SIGNAL(setRunningState(bool)),listener,SLOT(setRunningState(bool)));
    connect(this,SIGNAL(setCountingState(bool)),listener,SLOT(setCountingState(bool)));

    listener->start();

    //mFuture = QtConcurrent::run(listener,&(listener->countNum));
}


void Dialog::on_pB_Pause_clicked()
{
    emit setCountingState(false);
    // if(mFuture.isSuspended())
    //     mFuture.resume();
    // else
    //     mFuture.suspend();
}


void Dialog::on_pB_Resume_clicked()
{
    emit setCountingState(true);
    // if(mFuture.isStarted())
    //     mFuture.cancel();
}

void Dialog::on_value_set(int i)
{
    ui->progressBar->setValue(i);
}




void Dialog::on_pB_Stop_clicked()
{
    emit setRunningState(false);
    //listener->wait();
    // listener->deleteLater();
    // listener = NULL;
    //listener->wait();

}


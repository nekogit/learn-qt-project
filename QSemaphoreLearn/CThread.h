#pragma once

#include <QThread>
#include <QDebug>
#include <QString>
#include <QSemaphore>
#include <QRandomGenerator>

#include <stdlib.h>

static QSemaphore seme_(5);

class CThread : public QThread{
public :
    explicit CThread(){}
    ~CThread(){}



    // QThread interface
protected:
    void run() override{
        while(true)
        {
            qDebug()<<"From: "<<__FILE__<<__LINE__<<" "<<QThread::currentThread()<<" Trying to acquire a product,leave: "<<seme_.available();
            seme_.acquire(1);

            qDebug()<<"From: "<<__FILE__<<__LINE__<<" "<<QThread::currentThread()<<" Working...";
            QThread::sleep(QRandomGenerator::global()->bounded(1,6));

            qDebug()<<"From: "<<__FILE__<<__LINE__<<" "<<QThread::currentThread()<<" Released a product";
            seme_.release(1);
        }
    }
};



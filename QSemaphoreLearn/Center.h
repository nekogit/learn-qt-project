#pragma once

#include <QObject>
#include <QList>

#include "CThread.h"

class Center: public QObject
{
    Q_OBJECT

public:
    explicit Center(){
        for(int i = 0 ; i < 7 ; i++)
        {
            CThread *thread = new CThread;
            threads.append(thread);
            thread->start();
        }
    }
    ~Center(){
        for(int i = 0 ; i < 7 ; i++)
        {
            threads[i]->exit();
            threads[i]->deleteLater();
        }
    }

private:
    QList<CThread *> threads;
};



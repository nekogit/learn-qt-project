#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    resize(1000,800);
    pb_w1 = new QPushButton(this);
    pb_w1->setText("previous");

    pb_w2 = new QPushButton(this);
    pb_w2->setText("next");

    pb_w1->setGeometry(100,750,100,50);
    pb_w2->setGeometry(800,750,100,50);

    connect(pb_w1,&QPushButton::clicked,[]{
        qDebug() << "previous button clicked!";
    });

    connect(pb_w2,&QPushButton::clicked,[]{
        qDebug() << "next button clicked!";
    });
    connect(pb_w2,&QPushButton::clicked,this,&Widget::switch_widget);

    m_widget_1 = new FluentWidget(this);
    m_widget_1->setGeometry(100,50,800,600);
    //m_widget_1->setStyleSheet("#FluentWidget{background-color:rgb(25,25,25);}");

    m_widget_2 = new FluentWidget(this);
    m_widget_2->setGeometry(1100,50,800,600);


}

Widget::~Widget()
{
    delete pb_w1;
    delete pb_w2;
    delete m_widget_1;
    delete m_widget_2;
    delete ui;
}

void Widget::switch_widget()
{
    if(currentPage == 1)
    {
        qDebug() << "going to page 2,turn right...";
        QPropertyAnimation *turnrightAnimation = new QPropertyAnimation(m_widget_1,"geometry");
        //turnrightAnimation.setParent(this);
        // turnrightAnimation.setTargetObject(m_widget_1);
        // turnrightAnimation.setPropertyName("geometry");
        turnrightAnimation->setDuration(500);
        turnrightAnimation->setStartValue(QRect(100,50,800,600));
        turnrightAnimation->setEndValue(QRect(-900,50,800,600));
        //turnrightAnimation->setEasingCurve(QEasingCurve::InOutQuad);
        turnrightAnimation->start();
        //m_widget_1->hide();
        currentPage = 2;

        QPropertyAnimation *turnrightAnimation_2 = new QPropertyAnimation(m_widget_2,"pos",this);
        turnrightAnimation_2->setDuration(500);
        turnrightAnimation_2->setStartValue(QPoint(1100,50));
        turnrightAnimation_2->setEndValue(QPoint(100,50));
        turnrightAnimation_2->start();
        // delete turnrightAnimation_2;
    }else if(currentPage == 2)
    {

        qDebug() << "going to page 1,turn left...";
        QPropertyAnimation *turnleftAnimation = new QPropertyAnimation(m_widget_1,"geometry",this);
        turnleftAnimation->setDuration(500);
        turnleftAnimation->setStartValue(QRect(-900,50,800,600));
        turnleftAnimation->setEndValue(QRect(100,50,800,600));
        //turnrightAnimation->setEasingCurve(QEasingCurve::InOutQuad);
        turnleftAnimation->start();
        //m_widget_1->hide();
        //currentPage = 2;
        // QThread::sleep(5);
        // delete turnleftAnimation;
        turnleftAnimation = new QPropertyAnimation(m_widget_2,"pos",this);
        turnleftAnimation->setDuration(500);
        turnleftAnimation->setStartValue(QPoint(100,50));
        turnleftAnimation->setEndValue(QPoint(1100,50));
        turnleftAnimation->start();
        // QThread::sleep(1.5);
        // delete turnleftAnimation;
        currentPage = 1;
    }
}

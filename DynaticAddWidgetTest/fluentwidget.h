#ifndef FLUENTWIDGET_H
#define FLUENTWIDGET_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QDebug>

class FluentWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FluentWidget(QWidget *parent = nullptr);
    ~FluentWidget();

private:
    QPushButton * pb;
};

#endif // FLUENTWIDGET_H

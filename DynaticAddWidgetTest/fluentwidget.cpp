#include "fluentwidget.h"

FluentWidget::FluentWidget(QWidget *parent)
    : QWidget{parent}
{
    resize(800,600);
    pb = new QPushButton(this);
    pb->setText("click me!");
    pb->setGeometry(400,300,200,30);
    qDebug() << "FluentWidget Construction";
    connect(pb,&QPushButton::clicked,[&]{
        qDebug() << "a button clicked!";
    });
}


FluentWidget::~FluentWidget()
{
    qDebug() << "FluentWidget Destruction";
    delete pb;
    deleteLater();
}

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QDebug>
#include <QPropertyAnimation>
#include <QThread>

#include "fluentwidget.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private:
    Ui::Widget *ui;
    QPushButton *pb_w1;
    QPushButton *pb_w2;
    FluentWidget * m_widget_1;
    FluentWidget * m_widget_2;
    quint16 currentPage = 1;

private slots:
    void switch_widget();
};
#endif // WIDGET_H

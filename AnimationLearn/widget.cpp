﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);


    pb = new QPushButton("O",this);
    pb->setGeometry(QRect(this->width()/2,this->height()/2,30,30));

    PA = new QPropertyAnimation;
}

Widget::~Widget()
{
    delete pb;
    delete PA;
    delete ui;
}


void Widget::on_pB_Same_clicked()
{
    delete PA;
    PA = new QPropertyAnimation;
    PA->setTargetObject(pb);
    PA->setPropertyName("pos");
    PA->setDuration(2000);
    PA->setStartValue(QPoint(0,this->height()/2));
    PA->setEasingCurve(QEasingCurve::Linear);
    PA->setEndValue(QPoint(this->width()-20,this->height()/2));

    PA->start();

}

void Widget::on_pB_Accurate_clicked()
{
    delete PA;
    PA = new QPropertyAnimation;
    PA->setTargetObject(pb);
    PA->setPropertyName("pos");
    PA->setDuration(2000);
    PA->setStartValue(QPoint(0,this->height()/2));
    PA->setEasingCurve(QEasingCurve::InQuad);
    PA->setEndValue(QPoint(this->width()-20,this->height()/2));

    PA->start();

}

void Widget::on_pB_Linear_clicked()
{
    delete PA;
    PA = new QPropertyAnimation;
    PA->setTargetObject(pb);
    PA->setPropertyName("pos");
    PA->setDuration(2000);
    //PA->setEasingCurve(QEasingCurve::Linear);
    PA->setEasingCurve(QEasingCurve::InQuad);
    PA->setStartValue(QPoint(0,this->height()/2));
    PA->setKeyValueAt(0.3,QPoint(this->width()/2-60,this->height()-70));
    PA->setKeyValueAt(0.7,QPoint(this->width()/2+40,this->height()-20));
    PA->setEndValue(QPoint(this->width()-20,this->height()/2));

    PA->start();

}

void Widget::on_pB_pause_clicked()
{
    PA->pause();
}

void Widget::on_pB_resume_clicked()
{
    PA->resume();
}

void Widget::on_pB_Custom_clicked()
{
    delete PA;
    PA = new QPropertyAnimation;
    PA->setTargetObject(pb);
    PA->setPropertyName("pos");
    PA->setDuration(4000);
    PA->setEasingCurve(QEasingCurve::Linear);
    PA->setStartValue(QPoint(0,this->height()-20));

}

﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QPropertyAnimation>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_pB_Same_clicked();

    void on_pB_Accurate_clicked();

    void on_pB_Linear_clicked();

    void on_pB_pause_clicked();

    void on_pB_resume_clicked();

    void on_pB_Custom_clicked();

private:
    Ui::Widget *ui;
    QPushButton * pb;
    QPropertyAnimation *PA;
};
#endif // WIDGET_H

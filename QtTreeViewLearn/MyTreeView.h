﻿#ifndef MYTREEVIEW_H
#define MYTREEVIEW_H

#endif // MYTREEVIEW_H
#include <QObject>
#include <QTreeView>
#include <QMouseEvent>
#include <QDebug>

class MyTreeView: public QTreeView
{
    Q_OBJECT

public:
    explicit MyTreeView(QWidget *parent = nullptr);
    ~MyTreeView();


    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
};

MyTreeView::MyTreeView(QWidget *parent)
{

}

MyTreeView::~MyTreeView()
{

}

void MyTreeView::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug()<<QStringLiteral("左键被按下了");
    }
    else if(event->button() == Qt::RightButton)
    {
        qDebug()<<QStringLiteral("右键被按下了");
    }

    QModelIndex index = indexAt(event->pos());
    if(index.isValid()){
        qDebug()<<QStringLiteral("index is valid");
    }
    else
    {
        qDebug()<<QStringLiteral("index is invalid");
    }
}

//void MyTreeView::mouseReleaseEvent(QMouseEvent *event)
//{

//}

//void MyTreeView::mouseDoubleClickEvent(QMouseEvent *event)
//{

//}

//void MyTreeView::mouseMoveEvent(QMouseEvent *event)
//{

//}

﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QStandardItemModel>
#include <QStandardItem>
#include <QItemSelection>
#include <QItemSelectionModel>
#include <QString>
#include <QStringList>
#include <QStringLiteral>
#include <QDebug>
#include <QMenu>
#include <QIcon>
//#include "MyTreeView.h"
#include "mydelegate.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void IniMVC();

public slots:
    void slotSelectionChanged(const QItemSelection &selected,const QItemSelection &deselected);
    void slotcurrentChanged(const QModelIndex &t,const QModelIndex &previous);
    void slotCurrentRowChanged(const QModelIndex &t,const QModelIndex &previous);

    void slotTreeMenu(const QPoint &pos);
    void slotTreeMenuExpand(bool);
    void slotTreeMenuCollapse(bool);

private:
    Ui::Widget *ui;
    QStandardItemModel *mModel;

    // QWidget interface

};
#endif // WIDGET_H

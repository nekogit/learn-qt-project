﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    IniMVC();


    connect(ui->treeView->selectionModel(),SIGNAL(selectionChanged(QItemSelection,QItemSelection)),this,SLOT(slotSelectionChanged(QItemSelection,QItemSelection)));
    connect(ui->treeView->selectionModel(),SIGNAL(currentChanged(QModelIndex,QModelIndex)),this,SLOT(slotcurrentChanged(QModelIndex,QModelIndex)));
    connect(ui->treeView->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),this,SLOT(slotCurrentRowChanged(QModelIndex,QModelIndex)));


}

Widget::~Widget()
{
    delete ui;
}

void Widget::IniMVC()
{
    //    QStandardItemModel *model = new QStandardItemModel(ui->treeView);
    //    model->setHorizontalHeaderLabels(QStringList()<<QStringLiteral("序号")<<QStringLiteral("名称"));
    //    for(int i = 0;i < 5;i++)
    //    {
    //        QList<QStandardItem*> items1;
    //        QStandardItem * item1 = new QStandardItem(QString::number(i));
    //        QStandardItem * item2 = new QStandardItem(QStringLiteral("一级节点"));
    //        items1.append(item1);
    //        items1.append(item2);
    //        model->appendRow(items1);


    //        for(int j = 0;j < 5; j++)
    //        {
    //            QList<QStandardItem*>items2;
    //            QStandardItem* item3 = new QStandardItem(QString::number(j));
    //            QStandardItem* item4 = new QStandardItem(QStringLiteral("二级节点"));
    //            items2.append(item3);
    //            items2.append(item4);
    //            item1->appendRow(items2);

    //            for(int k = 0;k < 5; k++)
    //            {
    //                QList<QStandardItem *>items3;
    //                QStandardItem * item5 = new QStandardItem(QString::number(k));
    //                QStandardItem * item6 = new QStandardItem(QStringLiteral("三级节点"));
    //                items3.append(item5);
    //                items3.append(item6);
    //                item3->appendRow(items3);
    //            }
    //        }
    //    }

    //    ui->treeView->setModel(model);

        QTreeView *t =ui->treeView;

        MyDelegate *delegate = new MyDelegate(this);

        t->setItemDelegate(delegate);

        //t->setEditTriggers(QTreeView::NoEditTriggers);
        t->setEditTriggers(QTreeView::DoubleClicked);
        t->setSelectionBehavior(QTreeView::SelectRows);
        t->setSelectionMode(QTreeView::SingleSelection);
        t->setAlternatingRowColors(true);

        t->setFocusPolicy(Qt::NoFocus);

        t->header()->setStretchLastSection(true);
        t->header()->setDefaultAlignment(Qt::AlignCenter);

        QStandardItemModel* model = new QStandardItemModel(ui->treeView);
        model->setHorizontalHeaderLabels(QStringList()<<QStringLiteral("国家")<<QStringLiteral("省份")<<QStringLiteral("城市"));


        QList<QStandardItem *> items1;
        QStandardItem * item1 = new QStandardItem(QStringLiteral("中国"));
        QStandardItem * item2 = new QStandardItem(QStringLiteral("\\"));

        items1.append(item1);
        items1.append(item2);

        QList<QStandardItem *> items1_;
        QStandardItem * item1_item1 = new QStandardItem(QStringLiteral("↑"));
        QStandardItem * item1_item2 = new QStandardItem(QStringLiteral("四川省"));

        items1_.append(item1_item1);
        items1_.append(item1_item2);
        item1->appendRow(items1_);

        QList<QStandardItem *> items_city;
        QStandardItem * CityItem1 = new QStandardItem(QStringLiteral(" "));
        QStandardItem * CityItem2 = new QStandardItem(QStringLiteral("↑"));
        QStandardItem * CityItem3 = new QStandardItem(QStringLiteral("成都市"));

        items_city.append(CityItem1);
        items_city.append(CityItem2);
        items_city.append(CityItem3);

        item1_item1->appendRow(items_city);



        QList<QStandardItem *> items2;
        QStandardItem * item3 = new QStandardItem(QStringLiteral("英国"));
        QStandardItem * item4 = new QStandardItem(QStringLiteral("\\"));

        items2.append(item3);
        items2.append(item4);

        model->appendRow(items1);
        model->appendRow(items2);


        ui->treeView->setModel(model);

        t->setContextMenuPolicy(Qt::CustomContextMenu);
        connect(t,SIGNAL(customContextMenuRequested(QPoint)),this,SLOT(slotTreeMenu(QPoint)));

        t->header()->resizeSection(0,100);
        t->header()->setSectionResizeMode(0,QHeaderView::Fixed);

        QModelIndex rootIndex = t->rootIndex();
        QModelIndex selIndex = model->index(0,0,rootIndex);
        t->setCurrentIndex(selIndex);
        mModel = model;
}

void Widget::slotSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
    QItemSelectionModel *select = ui->treeView->selectionModel();

    QModelIndexList indexes = select->selectedIndexes();

    foreach (QModelIndex index,indexes) {
        QStandardItem *item = mModel->itemFromIndex(index);
        if(item)
        {
            ui->label->setText(item->text());
        }
    }
}

void Widget::slotcurrentChanged(const QModelIndex &t, const QModelIndex &previous)
{
    QStandardItem *item = mModel->itemFromIndex(t);
    if(item)
    {
        qDebug()<<QStringLiteral("SLOTCURRENTCHANGED");
    }

}

void Widget::slotCurrentRowChanged(const QModelIndex &t, const QModelIndex &previous)
{
    QStandardItem *item = mModel->itemFromIndex(t);
    if(item)
    {
        qDebug()<<QStringLiteral("SLOTCURRENTROWCHANGED");
    }
}

void Widget::slotTreeMenu(const QPoint &pos)
{
    QString qss = "QMenu{color:#E8E8E8;background:#4D4D4D;margin:2px;}\
                    QMenu::item{padding:3px 20px 3px 20px;}\
                    QMenu::indicator{width:13px;height:13px;}\
                    QMenu::item:selected{color:#E8E8E8;border:0px solid #575757;background:#1E90FF;}\
                    QMenu::separator{height:1px;background:#757575;}";

    QMenu menu;
    menu.setStyleSheet(qss);    //可以给菜单设置样式

    QModelIndex curIndex = ui->treeView->indexAt(pos);
    QModelIndex index = curIndex.sibling(curIndex.row(),0);
    if(index.isValid())
    {
//        menu.addAction(QIcon(":/images/images/menu.png"),QStringLiteral("展开"),this,SLOT(slotTreeMenuExpand(bool)));
//        menu.addSeparator();
//        menu.addAction(QIcon(":/images/images/mini.png"),QStringLiteral("折叠"),this,SLOT(slotTreeMenuCollapse(bool)));
        QAction * actionParent = menu.addAction(QStringLiteral("移动此栏"));    //父菜单

        QMenu *subMenu = new QMenu(&menu);
        subMenu->addAction(QStringLiteral("1"),this,SLOT(slotTreeMenuExpand(bool)));
        subMenu->addAction(QStringLiteral("2"),this,SLOT(slotTreeMenuExpand(bool)));
        subMenu->addAction(QStringLiteral("3"),this,SLOT(slotTreeMenuExpand(bool)));
        subMenu->addAction(QStringLiteral("4"),this,SLOT(slotTreeMenuExpand(bool)));

        actionParent->setMenu(subMenu);
    }
    menu.exec(QCursor::pos());

}

void Widget::slotTreeMenuExpand(bool checked)
{
    QModelIndex curIndex = ui->treeView->currentIndex();
    QModelIndex index = curIndex.sibling(curIndex.row(),0);
    if(index.isValid())
    {
        ui->treeView->expand(index);
    }
}

void Widget::slotTreeMenuCollapse(bool checked)
{
    QModelIndex curIndex = ui->treeView->currentIndex();
    QModelIndex index = curIndex.sibling(curIndex.row(),0);
    if(index.isValid())
    {
        ui->treeView->collapse(index);
    }
}




#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    athread = new AThread();

    connect(athread,SIGNAL(newValue(int,int)),this,SLOT(do_newValue(int,int)));

    athread->start();
}

Widget::~Widget()
{
    athread->terminate();
    athread->wait();
    delete athread;
    delete ui;
}


void Widget::on_pB_Start_clicked()
{
    //athread->start();
    athread->setThreadStart(true);
    ui->pB_Stop->setEnabled(true);
    //ui->pB_Clear->setEnabled(true);
    ui->pB_Resume->setEnabled(true);
    ui->pB_Start->setEnabled(false);
    ui->pB_TStop->setEnabled(true);
    DEBUG_CONTEXT(QString("ATHREAD START"));
}

void Widget::on_pB_Stop_clicked()
{
    athread->setThreadStart(false);
    athread->terminate();
    athread->wait();
    DEBUG_CONTEXT(QString("ATHREAD STOPED"));
    ui->pB_Start->setEnabled(true);
    ui->pB_Stop->setEnabled(false);
    ui->pB_Resume->setEnabled(false);
    ui->pB_TStop->setEnabled(false);
    athread->start();
}

void Widget::do_newValue(int count,int data)
{
    QString Dis = QString("ACCEPT ('")+QString::number(count)+QString("'),THE RESULT IS: ")+QString::number(data)+"\n";
    ui->TextDisplay->appendPlainText(Dis);
    ui->label->setText(QString::number(data));


    DEBUG_CONTEXT(QString("NEW VALUE FOUND"));
}

void Widget::DEBUG_CONTEXT(QString msg)
{
    QString output =QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")+QString("[WIDGET PROCESS]: ")+msg;
    output = output.toUtf8().data();

    qDebug()<<output;
}

void Widget::on_pB_Clear_clicked()
{
    ui->TextDisplay->clear();
    ui->label->setText(QString::number(0));
    DEBUG_CONTEXT(QString("TEXT CLEAR"));
}

void Widget::on_pB_Resume_clicked()
{
    athread->setThreadStart(true);
    DEBUG_CONTEXT(QString("RESUME TO GO"));
}

void Widget::on_pB_TStop_clicked()
{
    athread->setThreadStart(false);
    DEBUG_CONTEXT(QString("TEMP STOP"));
}

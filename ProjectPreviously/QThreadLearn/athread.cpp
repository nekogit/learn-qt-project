#include "athread.h"



AThread::AThread()
{
    DEBUG_CONTEXT(QString("ENTERING --ATHREAD--"));
}

void AThread::DEBUG_CONTEXT(QString msg)
{
    QString output =QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss")+QString("[ATHREAD PROCESS]: ")+msg;
    output = output.toUtf8().data();

    qDebug()<<output;
}

void AThread::setThreadStart(bool state)
{

    IF_THREAD_START = state;

}

void AThread::run()
{
    count = 0;
    current_result = -1;

    IF_THREAD_RUN = true;
    IF_THREAD_START = false;


    while(IF_THREAD_RUN)
    {
        if(IF_THREAD_START)
        {
            //产生随机数并发送信号
            current_result = QRandomGenerator::global()->bounded(1,10);
            count++;
            newValue(count,current_result);
            msleep(500);
        }
    }

    quit();
}

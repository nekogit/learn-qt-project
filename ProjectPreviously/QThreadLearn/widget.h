#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "athread.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    void DEBUG_CONTEXT(QString);

private slots:
    void on_pB_Start_clicked();

    void on_pB_Stop_clicked();

    void do_newValue(int count,int data);

    void on_pB_Clear_clicked();

    void on_pB_Resume_clicked();

    void on_pB_TStop_clicked();

private:
    Ui::Widget *ui;
    AThread *athread;

};
#endif // WIDGET_H

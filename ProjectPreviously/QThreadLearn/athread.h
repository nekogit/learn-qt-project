#ifndef ATHREAD_H
#define ATHREAD_H
#include <QThread>
#include <QObject>
#include <QDebug>
#include <QString>
#include <QDateTime>
#include <QRandomGenerator>
#include <QMutex>


class AThread : public QThread
{
    Q_OBJECT
public:
    AThread();

    void DEBUG_CONTEXT(QString);

    void setThreadStart(bool state);



private:
    int count;
    int current_result;

    bool IF_THREAD_RUN;
    bool IF_THREAD_START;

    QMutex mutex_1;

    // QThread interface
protected:
    void run();

signals:
    void newValue(int count,int data);


};

#endif // ATHREAD_H

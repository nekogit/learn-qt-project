#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    resize(2000,2000);
}

Widget::~Widget()
{
    delete ui;
}


//void Widget::paintEvent(QPaintEvent *event)
//{


//    int W = this->width();
//    int H = this->height();


//    //画点
//    QPainter *painter_points = new QPainter(this);

//    QPoint points[] = {
//        QPoint(W/10,H/10),
//        QPoint(W/6,H/6),
//        QPoint(W/5,H/5)
//    };

//    QPen pen_points;
//    pen_points.setWidth(10);
//    pen_points.setColor(Qt::red);
//    pen_points.setStyle(Qt::SolidLine);
//    pen_points.setCapStyle(Qt::FlatCap);
//    pen_points.setJoinStyle(Qt::MiterJoin);

//    painter_points->setPen(pen_points);

//    painter_points->drawPoints(points,3);

//    //画线
//    QPainter *painter_lines = new QPainter(this);

//    QRect rect_lines(100,100,100,100);

//    QVector<QLine> lines;
//    lines.append(QLine(rect_lines.topLeft(),rect_lines.bottomRight()));
//    lines.append(QLine(rect_lines.topRight(),rect_lines.bottomLeft()));

//    painter_lines->setPen(pen_points);

//    painter_lines->drawLines(lines);

//    //画矩形
//    QPainter *painter_rect = new QPainter(this);

//    QRect rect(W/6,H/4,W/3,W/3);

//    painter_rect->setPen(pen_points);

//    QBrush brush;
//    brush.setColor(Qt::red);
//    brush.setStyle(Qt::Dense6Pattern);

//    painter_rect->setBrush(brush);

//    painter_rect->drawRect(rect);

//    painter_rect->drawArc(rect,0,90*16);
//    painter_rect->drawArc(rect,180*16,90*16);
//    //painter_rect->drawEllipse(rect);

//    QFont font;
//    font.setPointSize(10);
//    font.setBold(true);

//    painter_rect->setFont(font);
//    painter_rect->drawText(rect.center(),"Hello");

//    event->accept();
//}

void Widget::paintEvent(QPaintEvent *event)
{
    QPainter *painter = new QPainter(this);

    int W = this->width();
    int H = this->height();

    QRect rect(W/5,H/5,W/5,H/5);

    QPen pen;
    pen.setColor(Qt::black);
    pen.setWidth(3);

    painter->setPen(pen);

    QPainterPath path;

    path.addEllipse(rect);
    path.addRect(rect);

    //painter->drawPath(path);
    painter->fillPath(path,Qt::black);

    painter->save();

    painter->translate(200,200);
    painter->fillPath(path,Qt::black);

    painter->restore();

    //painter->translate(0,200);
    painter->scale(1.2,1.2);
    painter->rotate(20);

    painter->fillPath(path,Qt::black);

    painter->resetTransform();

    event->accept();
}


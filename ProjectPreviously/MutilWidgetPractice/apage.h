#ifndef APAGE_H
#define APAGE_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
class widget;
class APage : public QWidget
{
    Q_OBJECT
public:
    explicit APage(QWidget *parent = nullptr);
    ~APage();

//signals:
public slots:
    void do_Click_PB();

private:
    widget *wgt;
    QPushButton *pb;
    QHBoxLayout *hl1;

};

#endif // APAGE_H

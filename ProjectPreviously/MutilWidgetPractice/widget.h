#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QDebug>
#include "apage.h"

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void saysth();

public slots:

    void do_Click_PB();

private:
    QPushButton *pb;
    QHBoxLayout *hl1;
    APage pg1;
};
#endif // WIDGET_H

#include "widget.h"
#include "mydlg.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv); //传参,main将主导权递交给QApplication(QT对象)
    Widget w;   //主界面窗口对象创建

    Mydlg my1;  //初始页面窗口对象创建

    if(my1.exec() == QDialog::Accepted) //进入初始页面窗口exec的等待循环,当循环结果返回了accepted进入
    {
        w.show();   //主界面显示
        return a.exec();
    }

    //w.show();
    //return a.exec();
    else    //未进入而结束my1.exec的循环等待,跳入else语句结束程序
    return 0;
}

#include "myitem.h"

MyItem::MyItem()
{

}


QRectF MyItem::boundingRect() const
{
    return QRectF(0,0,200,200);
}

void MyItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    QBrush brush;
    QRect rect(0,0,200,200);
    brush.setColor(Qt::red);
    brush.setStyle(Qt::Dense6Pattern);

    painter->setBrush(brush);

    painter->drawArc(rect,0,360*16);
}

#ifndef MYITEM_H
#define MYITEM_H

//#include <QObject>
//#include <QWidget>
#include <QGraphicsItem>
#include <QPainter>
#include <QRect>
#include <QBrush>


class MyItem :public QGraphicsItem
{

public:
    MyItem();

    // QGraphicsItem interface
public:
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // MYITEM_H

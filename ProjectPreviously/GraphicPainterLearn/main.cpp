//#include "widget.h"
#include "myitem.h"
#include <QApplication>
#include <QGraphicsView>
#include <QGraphicsScene>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
//    Widget w;
//    w.show();

    QGraphicsScene scene;

    scene.setSceneRect(-200,-200,400,400);

    QGraphicsView view;

    view.setScene(&scene);

    MyItem *myitem = new MyItem();

    myitem->setPos(0,0);

    scene.addItem(myitem);

    view.show();


    return a.exec();
}

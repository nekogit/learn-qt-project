import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.0
import QtQuick.Dialogs 1.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("模拟文件系统")

    Connections{
        target: filecase

        onCreate_file_signal:{
            console.log("发送创建信号成功");
            filecase.new_file(file_msg.text);
            //info_text.text = qsTr("已创建文件")
        }

        onWrite_file_signal:{
            console.log("发送写入信号成功");
            filecase.write_file(file_msg.text);
            //info_text.text = qsTr("已写入文件")
        }

        onDelete_file_signal:{
            console.log("发送删除信号成功");
            filecase.delete_file(file_msg.text);
            //info_text.text = qsTr("已删除文件")
            file_msg.clear();
        }

        onOpen_file_signal:{
            console.log("发送打开信号成功");
            filedialog.visible = true;
            //file_msg.text = filecase.open_file(file_msg.text);

        }

        onClose_file_signal:{
            console.log("发送关闭信号成功");
            file_msg.clear();
            //info_text.text = qsTr("已清空显示框")
        }

        onMsg_file_signal:{
            console.log("发送显示文件属性信号成功");
            file_msg.text = filecase.show_file_info();
        }
    }


    File_newAction{
        id:file_new

        anchors.left: parent.left;
        anchors.leftMargin: 10;
        anchors.top: parent.top;
        anchors.topMargin: 10;

    }

    File_openAction{
        id:file_open

        anchors.left: file_new.right;
        anchors.leftMargin: 260;
        //anchors.centerIn: parent;
        anchors.top: parent.top;
        anchors.topMargin: 10;
    }

    File_writeAction{
        id:file_write

        anchors.left: file_open.right
        anchors.leftMargin: 260;
        anchors.top: parent.top
        anchors.topMargin: 10;
    }

    File_closeAction{
        id:file_close

        anchors.left: parent.left;
        anchors.leftMargin: 10;
        anchors.top:file_new.bottom;
        anchors.topMargin: 100;
    }

    File_deleteAction{
        id:file_delete

        anchors.left: file_close.right;
        anchors.leftMargin: 260;
        anchors.top:file_open.bottom;
        anchors.topMargin: 100;
    }

    File_msgAction{
        id:file_msg_bt

        anchors.left: file_delete.right
        anchors.leftMargin: 260;
        anchors.top:file_write.bottom;
        anchors.topMargin: 100;
    }


    Rectangle{
        id:file_msg_rec
        width: parent.width
        height: 280
        x:0
        y:200
        color:"transparent"

        border.width: 1
        TextArea{
            id:file_msg

            placeholderText: qsTr("打开的文件内文字显示")

            font.bold: true;
            font.pointSize: 15

        }
    }

    FileDialog{
        id:filedialog
        title: "选择文件夹"
        folder: shortcuts.desktop
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        visible: false
        onAccepted: {
            console.log("选择路径下文件");
            file_msg.text = filecase.open_file(filedialog.fileUrl);
            info_text.text = qsTr("已打开文件");
        }
    }

//    Rectangle{
//        id:info_rec

//        anchors.bottom: file_msg_rec.top;
//        anchors.bottomMargin: 30;

//        anchors.right: parent.right;
//        anchors.rightMargin: 20;

//        width: 60;
//        height:30;

//        Text{
//            id:info_text
//            text: qsTr("提示用文字")
//            font.bold: true;
//            font.pointSize: 15;
//            color: "red";
//        }

//    }


}

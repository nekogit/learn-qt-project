import QtQuick 2.0

Item {
    Rectangle{//删除文件操作按钮
        id:newfilebt;
        width: 100;
        height: 60;
        color: "blue";


        border.width: 1;

        Text{
            id:label_new
            text: qsTr("删除文件");
            font.pointSize: 15;

            anchors.centerIn: parent
        }

        MouseArea{
            id:mousearea_newfile
            hoverEnabled: true;
            anchors.fill: parent;
            onEntered: {
                newfilebt.color = "green"
            }
            onExited: {
                newfilebt.color = "blue"
            }
            onPressed: {
                newfilebt.color = "green";
                console.log("删除文件按钮触发");
                filecase.delete_file_signal();
            }
        }

    }
}

import QtQuick 2.0

Item {
    Rectangle{//写入文件操作按钮
        id:newfilebt;
        width: 100;
        height: 60;
        color: "blue";


        border.width: 1;

        Text{
            id:label_new
            text: qsTr("文件属性");
            font.pointSize: 15;

            anchors.centerIn: parent
        }

        MouseArea{
            id:mousearea_newfile
            hoverEnabled: true;
            anchors.fill: parent;
            onEntered: {
                newfilebt.color = "green"
            }
            onExited: {
                newfilebt.color = "blue"
            }
            onPressed: {
                newfilebt.color = "green"
                console.log("文件属性按钮触发");
                filecase.msg_file_signal();
            }
        }

    }
}

import QtQuick 2.0

Item {

    //property alias mousearea_newfile;

    Rectangle{//新建文件操作按钮
        id:newfilebt;
        width: 100;
        height: 60;
        color: "blue";


        border.width: 1;

        Text{
            id:label_new
            text: qsTr("新建文件");
            font.pointSize: 15;

            anchors.centerIn: parent
        }

        MouseArea{
            id:mousearea_newfile
            hoverEnabled: true;
            anchors.fill: parent;
            onEntered: {
                newfilebt.color = "green"
            }
            onExited: {
                newfilebt.color = "blue"
            }
            onPressed: {
                newfilebt.color = "green";
                console.log("新建文件按钮触发");
                //通过信号创建文件,后面新建则覆盖
                filecase.create_file_signal();
            }
        }

    }
}

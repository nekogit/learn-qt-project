import QtQuick 2.0

Item {
    Rectangle{//打开文件操作按钮
        id:newfilebt;
        width: 100;
        height: 60;
        color: "blue";


        border.width: 1;

        Text{
            id:label_new
            text: qsTr("打开文件");
            font.pointSize: 15;

            anchors.centerIn: parent
        }

        MouseArea{
            id:mousearea_newfile
            hoverEnabled: true;
            anchors.fill: parent;
            onEntered: {
                newfilebt.color = "green"
            }
            onExited: {
                newfilebt.color = "blue"
            }
            onPressed: {
                newfilebt.color = "green";
                console.log("打开文件按钮触发");
                filecase.open_file_signal();
            }
        }

    }
}

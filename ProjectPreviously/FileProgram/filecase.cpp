#include "filecase.h"

FileCase::FileCase()
{

}

void FileCase::new_file(QString filename)
{
    QString filename_current;
    qDebug()<<QString("检测到的输入为:")<<filename;
    if(NULL == filename)
    {
        qDebug()<<"检测为NULL,自动创建文件";
        filename_current = QDateTime::currentDateTime().toString("yyyy-MM-dd-hh-mm-ss");
        qDebug()<<filename_current;
    }
    else
    {
        filename_current = filename;
    }

    QString file_path = QCoreApplication::applicationDirPath()+"/saves/"+filename_current+".txt";

    QFile *t_file = new QFile(file_path);

    bool ret = t_file->exists();    //因未写入,此步无效

    if(true == ret)
    {
        //qDebug()<<"文件创建成功";
    }
    else
    {
        //qDebug()<<"文件创建失败";
    }

    m_current_file_name = file_path;

    m_file = t_file;

}

QString FileCase::open_file(QString filename)
{
    if(NULL == filename)
    {
        QFile *t_file = m_file;
        t_file->open(QIODevice::ReadOnly);

        QString tmp_msg = QString(t_file->readAll());

        t_file->close();

        return tmp_msg;
    }
    else
    {
        QString file_path = filename.right(filename.size()-8)/*.replace("/","//")*/;

        QFile *t_file_2 = new QFile(file_path);

        t_file_2->open(QIODevice::ReadOnly);

        QString tmp_msg = QString(t_file_2->readAll());

        t_file_2->close();

        m_file = t_file_2;

        m_current_file_name = file_path;

        return tmp_msg;
    }
}

void FileCase::write_file(QString msg)
{
    qDebug()<<msg;

    QFile* current_file = m_file;

    bool ret_open_file = current_file->open(QIODevice::WriteOnly);

    current_file->write(msg.toStdString().c_str());

    current_file->close();
}

void FileCase::close_file(QString filename)
{
    if(NULL == filename)
    {
        QFile *file = m_file;

        delete file;
    }
    else
    {
        //...预留
    }

}

void FileCase::delete_file(QString filename)
{
//    if(NULL == filename)
//    {
        QFile::remove(m_current_file_name);
//    }
//    else
//    {
//        //...预留
//        QFileInfo fileinfo(filename);
//        if(fileinfo.isFile())
//        {
//            QFile::remove(filename);
//        }
//    }
}

QString FileCase::show_file_info()
{
    QFileInfo fileinfo(m_current_file_name);

    QString info = QString("当前文件属性为:\n");

    info.append("NAME: "+fileinfo.fileName()+"\n");
    info.append("PATH: "+fileinfo.filePath()+"\n");
    info.append("LINK: "+fileinfo.readLink()+"\n");
    info.append("OWNER: "+fileinfo.owner()+"\n");
    info += (QString("标记:")+QString("ycy"));
    info.append(QString::number(202113160609));

    return info;
}

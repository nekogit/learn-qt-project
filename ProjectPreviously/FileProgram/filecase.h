#ifndef FILECASE_H
#define FILECASE_H

#include <QObject>
#include <QFile>
#include <QDebug>
#include <QDateTime>
#include <QCoreApplication>
#include <QByteArray>
#include <QFileInfo>

class FileCase: public QObject //此类用作文件的直接操作类
{
    Q_OBJECT
public:
    FileCase();

    Q_INVOKABLE void new_file(QString filename);    //新建对应路径文件

    Q_INVOKABLE QString open_file(QString filename);   //打开对应路径文件

    Q_INVOKABLE void close_file(QString filename);  //关闭对应路径文件

    Q_INVOKABLE void write_file(QString msg);  //写入文件信息

    Q_INVOKABLE void delete_file(QString filename); //删除文件

    Q_INVOKABLE QString show_file_info();   //显示文件属性

signals:
    void create_file_signal();
    void delete_file_signal();
    void write_file_signal();
    void open_file_signal();
    void close_file_signal();
    void msg_file_signal();


private:
    QString m_current_file_name;
    QFile *m_file;
};

#endif // FILECASE_H

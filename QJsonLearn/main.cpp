﻿#include <QCoreApplication>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonParseError>
#include <QFile>
#include <QTextStream>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//    QJsonObject rootObject;

//    rootObject.insert("name",QString::fromLocal8Bit("老王"));
//    rootObject.insert("age",26);

//    QJsonObject o2;
//    o2.insert("basketball",QString::fromLocal8Bit("篮球"));
//    o2.insert("badminton",QString::fromLocal8Bit("羽毛球"));
//    rootObject.insert("interest",o2);

//    QJsonArray ac;
//    ac.append("black");
//    ac.append("white");
//    rootObject.insert("color",ac);

//    QJsonArray a3;
//    QJsonObject a3_o1;
//    a3_o1.insert("game",QString::fromLocal8Bit("三国杀"));
//    a3_o1.insert("price",58.5);
//    QJsonObject a3_o2;
//    a3_o2.insert("game",QString::fromLocal8Bit("海岛奇兵"));
//    a3_o2.insert("price",66.65);

//    a3.append(a3_o1);
//    a3.append(a3_o2);

//    rootObject.insert("like",a3);

//    QJsonObject o3;
//    QJsonObject o3_o1;
//    QJsonObject o3_o2;

//    o3_o1.insert("language",QString::fromLocal8Bit("汉语"));
//    o3_o1.insert("grade",10);
//    o3_o2.insert("language",QString::fromLocal8Bit("英语"));
//    o3_o2.insert("grade",6);

//    o3.insert("serialone",o3_o1);
//    o3.insert("serialtwo",o3_o2);

//    rootObject.insert("languages",o3);

//    rootObject.insert("vip",true);
//    rootObject.insert("address",QJsonValue::Null);

//    QJsonDocument doc;
//    doc.setObject(rootObject);

    QFile file("D://Qt//QtSave//QJsonLearn//js.json");

    if(!file.open(QIODevice::ReadOnly | QFile::Text)){
        qDebug()<<"error";
        return 0;
    }

//        if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
//            qDebug()<<"error";
//            return 0;
//        }



    QTextStream stream(&file);
    stream.setCodec("UTF-8");

//    stream << doc.toJson();

//    file.close();

    QString str = stream.readAll();
    file.close();

    QJsonParseError jsonError;
    QJsonDocument doc = QJsonDocument::fromJson(str.toUtf8(),&jsonError);

    if(jsonError.error != QJsonParseError::NoError && !doc.isNull()){
        qDebug()<<"json error: "<<jsonError.error;
        return 0;
    }

    QJsonObject rootObject = doc.object();

    rootObject["name"] = QString::fromLocal8Bit("老李");
    rootObject["vip"] = false;

    QJsonArray colorArray_t = rootObject["color"].toArray();
    colorArray_t.replace(0,"blue");
    colorArray_t.replace(1,"green");
    rootObject["color"] = colorArray_t;



    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)){
        qDebug()<<"error";
        return 0;
    }

    QTextStream stream2(&file);
    stream2.setCodec("UTF-8");

    QJsonDocument doc2;
    doc2.setObject(rootObject);

    stream2 << doc2.toJson();

    file.close();

    QJsonValue nameValue = rootObject.value("name");
    qDebug()<<"name: "<<nameValue.toString();

    QJsonValue ageValue = rootObject.value("age");
    qDebug()<<"age: "<<ageValue.toInt();

    QJsonValue vipValue = rootObject.value("vip");
    qDebug()<<"vip: "<<vipValue.toBool();

    QJsonValue addressValue = rootObject.value("address");
    if(addressValue.type() == QJsonValue::Null)
    {
        qDebug()<<"address = NULL";
    }

    QJsonValue interestValue = rootObject.value("interest");

    if(interestValue.type() == QJsonValue::Object)
    {
        QJsonObject interestObject = interestValue.toObject();
        QJsonValue badmintonValue = interestObject.value("badminton");
        QJsonValue basketballValue = interestObject.value("basketball");
        qDebug()<<" badminton: "<<badmintonValue.toString()<<"\n"
               <<"basketball: "<<basketballValue.toString();
    }

    QJsonValue  colorValue = rootObject.value("color");

    if(colorValue.type() == QJsonValue::Array)
    {
        QJsonArray colorArray = colorValue.toArray();

        for(int i = 0;i < colorArray.size();i++)
        {
            QJsonValue color = colorArray.at(i);
            qDebug()<<" color :"<<color.toString();
        }
    }

    QJsonValue likeValue = rootObject.value("like");

    if(likeValue.type() == QJsonValue::Array)
    {
        QJsonArray likeArray = likeValue.toArray();

        for(int i = 0; i < likeArray.size(); i++)
        {
            QJsonValue likeSValue = likeArray.at(i);

            if(likeSValue.type() == QJsonValue::Object)
            {
                QJsonObject likeSObject = likeArray.at(i).toObject();
                QJsonValue gameValue = likeSObject.value("game");
                QJsonValue priceValue = likeSObject.value("price");
                qDebug()<<"game: "<<gameValue.toString()
                       <<"price: "<<priceValue.toDouble();
            }
        }
    }

    QJsonObject languageObject = rootObject.value("languages").toObject();

    QJsonObject serialOneObject = languageObject.value("serialone").toObject();
    QJsonValue gradeValue = serialOneObject.value("grade");
    QJsonValue languageValue = serialOneObject.value("language");
    qDebug()<<"grade: "<<gradeValue.toInt()
           <<"language: "<<languageValue.toString();


//    QJsonObject setNameObject = rootObject.value("name").toObject();
//    rootObject["name"] = QString::fromLocal8Bit("老李");
//    rootObject["vip"] = false;


    return a.exec();
}

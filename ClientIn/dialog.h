#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QTcpSocket>
#include <QHostInfo>
#include <QHostAddress> //QHostAddress的静态成员函数与枚举类使用
#include <QString>
#include <QDataStream>
#include <QMessageBox>
#include <QNetworkProxy>

QT_BEGIN_NAMESPACE
namespace Ui {
class Dialog;
}
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:

    void readAction();  //此函数将作为处理接收的readReady信号的槽函数执行,本实例仅输出服务端产生并发出的文字
    void displayError(QAbstractSocket::SocketError error);  //作为socket的error信号绑定的槽函数,用于显式输出错误信息

    void on_pB_Connect_clicked();

    void readAction_2();    //同readAction函数


private:
    Ui::Dialog *ui;

    QString peerAddress;    //server端的地址信息字符串
    QString peerPort;   //server端的地址端口字符串

    QString hostAddress;    //Q_UNUSED

    QTcpSocket *socket = nullptr;   //本例使用的socket,初始化设置nullptr防止野指针(或NULL)
    QDataStream stream; //对IODevice转换的流处理设备,本例仅为转换字符串使用
};
#endif // DIALOG_H

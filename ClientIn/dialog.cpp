#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
    , socket(new QTcpSocket(this))
{
    ui->setupUi(this);

    peerAddress = QString::fromLocal8Bit("192.168.29.128");
    peerPort = QString::number(11405);

    stream.setDevice(socket);   //设置流设备操作的IODevice,QTcpSocket为QIODevice继承的类
    stream.setVersion(QDataStream::Qt_5_12);    //QDataStream与时俱进的格式变换,最好设置一下

    QNetworkProxyFactory::setUseSystemConfiguration(false); //对系统设置做更改,否则因为系统原因无法连接

    connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readAction_2()));

    //connect(socket,&QTcpSocket::disconnected,socket,&QObject::deleteLater);




}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::readAction()
{
    stream.startTransaction();  //开启类似缓冲的区域,以防数据存储出现意外的情况而写入失败

    QString message;
    stream >> message;

    if(!stream.commitTransaction()) //提交本地缓冲,确保数据写入成功
        return ;

    ui->textEdit->append(message);

}

void Dialog::displayError(QAbstractSocket::SocketError error)
{
    switch(error){
    case QAbstractSocket::RemoteHostClosedError:
        QMessageBox::information(this,tr("Client Information"),tr("RemoteHostClosedError"));    break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this,tr("Client Information"),tr("HostNotFoundError"));    break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this,tr("Client Information"),tr("ConnectionRefusedError"));   break;
    default:
        QMessageBox::information(this,tr("Client Information"),tr("the following error happens: %1").arg(socket->errorString()));

    }

}

void Dialog::on_pB_Connect_clicked()
{
    socket->abort();    //忽略上一个连接,类似清空的操作
    socket->connectToHost(peerAddress,peerPort.toInt());    //封装的连接server端操作

}

void Dialog::readAction_2()
{
    stream.startTransaction();

    QString message;

    stream >> message;

    if(!stream.commitTransaction())
    {
        return ;
    }

    ui->textEdit->append(message);
}


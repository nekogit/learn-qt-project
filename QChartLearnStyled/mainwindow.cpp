﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->chartView);
    lab_chartXY = new QLabel("Chart X=  ,  Y=  ");
    lab_chartXY->setMinimumWidth(200);
    ui->statusbar->addWidget(lab_chartXY);
    lab_hoverXY = new QLabel("Hovered X=  ,Y=  ");
    lab_hoverXY->setMinimumWidth(200);
    ui->statusbar->addWidget(lab_hoverXY);
    lab_clickXY = new QLabel("Clicked X=  ,Y=  ");
    lab_clickXY->setMinimumWidth(200);
    ui->statusbar->addWidget(lab_clickXY);
    createChart();
    prepareData();
    connect(ui->chartView,SIGNAL(mouseMovePoint(QPoint)),this,SLOT(do_mouseMovePoint(QPoint)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createChart()
{
    chart = new QChart();
    ui->chartView->setChart(chart);
    ui->chartView->setRenderHint(QPainter::Antialiasing);
    ui->chartView->setCursor(Qt::CrossCursor);

    QLineSeries *series0 = new QLineSeries();
    series0->setName(QStringLiteral("LineSeries 曲线"));
    series0->setPointsVisible(true);
//    series0->setMarkerSize(5);
//    series0->setSelectedColor(Qt::blue);
    connect(series0,SIGNAL(clicked(QPointF)),this,SLOT(do_series_clicked(QPointF)));
    connect(series0,SIGNAL(hovered(QPointF,bool)),this,SLOT(do_series_hovered(QPointF,bool)));

    QSplineSeries *series1 = new QSplineSeries();
    series1->setName(QStringLiteral("SplineSeries 曲线"));
    series1->setPointsVisible(true);
    //series1->setMarkerSize(5);
    //series1->setSelectedColor(Qt::blue);
    connect(series1,SIGNAL(clicked(QPointF)),this,SLOT(do_series_clicked(QPointF)));
    connect(series1,SIGNAL(hovered(QPointF,bool)),this,SLOT(do_series_hovered(QPointF,bool)));

    QPen pen(Qt::black);
    pen.setStyle(Qt::DotLine);
    pen.setWidth(2);
    series0->setPen(pen);
    pen.setStyle(Qt::SolidLine);
    series1->setPen(pen);
    chart->addSeries(series0);
    chart->addSeries(series1);

    QValueAxis *axisX = new QValueAxis;
    axisX->setRange(0,10);
    axisX->setLabelFormat("%.1f");
    axisX->setTickCount(11);
    axisX->setMinorTickCount(2);
    axisX->setTitleText("time(secs)");

    QValueAxis *axisY = new QValueAxis;
    axisY->setRange(-2,2);
    axisY->setLabelFormat("%.2f");
    axisY->setTickCount(5);
    axisY->setMinorTickCount(2);
    axisY->setTitleText("value");

    chart->addAxis(axisX,Qt::AlignBottom);
    chart->addAxis(axisY,Qt::AlignLeft);
    series0->attachAxis(axisX);
    series0->attachAxis(axisY);
    series1->attachAxis(axisX);
    series1->attachAxis(axisY);
    foreach (QLegendMarker* marker, chart->legend()->markers()) {
        connect(marker,SIGNAL(clicked()),this,SLOT(do_legendMarkerClicked()));
    }

}

void MainWindow::prepareData()
{
    QLineSeries *series0 = (QLineSeries *)chart->series().at(0);
    QSplineSeries *series1 = (QSplineSeries *)chart->series().at(1);
    qreal t = 0 , y1,y2 ,intv =0.5;
    int cnt = 21;
    for(int i = 0;i < cnt;i++)
    {
        int rd = QRandomGenerator::global()->bounded(-5,6);
        y1 = qSin(2*t) + rd/50;
        series0->append(t,y1);
        rd = QRandomGenerator::global()->bounded(-5,6);
        y2 = qSin(2*t + 20) + rd/50;
        series1->append(t,y2);
        t+=intv;
    }
}

void MainWindow::do_legendMarkerClicked()
{

}

void MainWindow::do_mouseMovePoint(QPoint point)
{
    QPointF pt = chart->mapToValue(point);
    QString str = QString::asprintf("Chart X=%.1f,Y=%.2f",pt.x(),pt.y());
    lab_chartXY->setText(str);
}

void MainWindow::do_series_clicked(const QPointF &point)
{
    QString str = QString::asprintf("Clicked X=%.1f,Y=%.2f",point.x(),point.y());
    lab_clickXY->setText(str);
    QLineSeries *series = qobject_cast<QLineSeries*>(sender());
    int index = getIndexFromX(series,point.x());
    if(index < 0)
        return;
    //bool isSelected = series->isPoint
}

void MainWindow::do_series_hovered(const QPointF &point, bool state)
{
    QString str = "Series X=, Y=";
    if(state)
        str = QString::asprintf("Hovered X=%.1f,Y=%.2f",point.x(),point.y());
    lab_hoverXY->setText(str);
    QLineSeries *series = qobject_cast<QLineSeries*>(sender());
    QPen pen = series->pen();
    if(state)
        pen.setColor(Qt::red);
    else
        pen.setColor(Qt::black);
    series->setPen(pen);
}

int MainWindow::getIndexFromX(QXYSeries *series, qreal xValue, qreal tol)
{
    QList<QPointF> points = series->points();
    int index = -1;
    for(int i = 0;i < points.count();i++)
    {
        qreal dx = xValue - points.at(i).x();
        if(qAbs(dx) <= tol)
        {
            index = i;
            break;
        }
    }
    return index;
}

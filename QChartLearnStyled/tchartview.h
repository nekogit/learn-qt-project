﻿#ifndef TCHARTVIEW_H
#define TCHARTVIEW_H

#include <QWidget>
#include <QLineSeries>
#include <QValueAxis>
#include <QChart>
#include <QChartView>

class TChartView: public QtCharts::QChartView
{
    Q_OBJECT
public:
    TChartView(QWidget *parent = nullptr);
    ~TChartView();
    void setCustomZoomRect(bool custom);

signals:
    void mouseMovePoint(QPoint point);

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

private:
    QPoint beginPoint;
    QPoint endPoint;
    bool m_customZoom = false;
};

#endif // TCHARTVIEW_H

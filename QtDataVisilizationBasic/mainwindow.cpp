﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //iniGraph3DP();
    //iniScatter3D();
    iniSurface3D();
    QSplitter *splitter = new QSplitter(Qt::Horizontal);
    splitter->addWidget(graphContainer);
    this->setCentralWidget(splitter);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::iniGraph3D()
{
    graph3D = new Q3DBars();
    graphContainer = QWidget::createWindowContainer(graph3D,this);
    graph3D->scene()->activeCamera()->setCameraPreset(QtDataVisualization::Q3DCamera::CameraPresetFrontHigh);

    QtDataVisualization::QValue3DAxis *axisV = new QtDataVisualization::QValue3DAxis;
    axisV->setTitle(QStringLiteral("销量"));
    axisV->setTitleVisible(true);
    axisV->setLabelFormat("%d");
    graph3D->setValueAxis(axisV);

    QtDataVisualization::QCategory3DAxis *axisRow = new QtDataVisualization::QCategory3DAxis;
    axisRow->setTitle(QStringLiteral("row axis"));
    axisRow->setTitleVisible(true);
    graph3D->setRowAxis(axisRow);

    QtDataVisualization::QCategory3DAxis *axisCol = new QtDataVisualization::QCategory3DAxis;
    axisCol->setTitle(QStringLiteral("col axis"));
    axisCol->setTitleVisible(true);
    graph3D->setColumnAxis(axisCol);

    series = new QtDataVisualization::QBar3DSeries;
    series->setMesh(QtDataVisualization::QAbstract3DSeries::MeshCylinder);
    series->setItemLabelFormat("(@rowLabel,@colLabel): %d");
    graph3D->addSeries(series);

    QtDataVisualization::QBarDataArray *dataArray = new QtDataVisualization::QBarDataArray;
    for(int i = 0 ; i < 3 ; i++)
    {
        QtDataVisualization::QBarDataRow *dataRow = new QtDataVisualization::QBarDataRow;
        for(int j = 0 ; j < 5 ; j++)
        {
            QtDataVisualization::QBarDataItem *item = new QtDataVisualization::QBarDataItem;
            quint32 value = QRandomGenerator::global()->bounded(5,15);
            item->setValue(value);
            dataRow->append(*item);
        }
        dataArray->append(dataRow);
    }

    QStringList rowLabs;
    rowLabs<<"Week1"<<"Week2"<<"Week3";
    series->dataProxy()->setRowLabels(rowLabs);
    QStringList colLabs;
    colLabs<<"Mon"<<"Tue"<<"Wed"<<"Thur"<<"Fri";
    series->dataProxy()->setColumnLabels(colLabs);
    series->dataProxy()->resetArray(dataArray);
}

void MainWindow::iniGraph3DP()
{
    graph3D = new Q3DBars();
    graphContainer = QWidget::createWindowContainer(graph3D,this);

    graph3D->scene()->activeCamera()->setCameraPreset(Q3DCamera::CameraPresetDirectlyAbove);
    //graph3D->scene()->activeCamera()->setCameraPreset(QtDataVisualization::Q3DCamera::CameraPresetFrontHigh);

    QValue3DAxis *axisY = new QValue3DAxis();
    axisY->setTitle(QStringLiteral("销量"));
    axisY->setTitleVisible(true);
    axisY->setLabelFormat("%.2d");
    graph3D->setValueAxis(axisY);

    QCategory3DAxis *axisRow = new QCategory3DAxis();
    axisRow->setTitle(QStringLiteral("RowAxis"));
    axisRow->setTitleVisible(true);
    graph3D->setRowAxis(axisRow);

    QCategory3DAxis *axisCol = new QCategory3DAxis();
    axisCol->setTitle(QStringLiteral("ColAxis"));
    axisCol->setTitleVisible(true);
    graph3D->setColumnAxis(axisCol);

    series = new QBar3DSeries();
    series->setMesh(QAbstract3DSeries::MeshCylinder);
    series->setItemLabelFormat("(@rowLabel,@colLabel): %.2d");
    graph3D->addSeries(series);

    QBarDataArray *dataArray = new QBarDataArray();
    for(int i = 0 ; i < 3 ;i++)
    {
        QBarDataRow * dataRow = new QBarDataRow();
        for(int j = 0 ; j < 5 ; j++)
        {
            QBarDataItem * dataItem = new QBarDataItem();
            quint32 value = QRandomGenerator::global()->bounded(5,50);
            qDebug()<<value;
            dataItem->setValue(value);
            dataRow->append(*dataItem);
        }
        dataArray->append(dataRow);
    }

    QStringList rowLabs;
    rowLabs<<"Week1"<<"Week2"<<"Week3";
    series->dataProxy()->setRowLabels(rowLabs);
    QStringList colLabs;
    colLabs<<"Mon"<<"Tue"<<"Wed"<<"Thur"<<"Fri";
    series->dataProxy()->setColumnLabels(colLabs);
    series->dataProxy()->resetArray(dataArray);
}

void MainWindow::iniScatter3D()
{
    scatter3D = new Q3DScatter();
    graphContainer = QWidget::createWindowContainer(scatter3D,this);
    QScatterDataProxy *proxy = new QScatterDataProxy;
    scatterSeries = new QScatter3DSeries(proxy);
    scatterSeries->setItemLabelFormat("(@xLabel,@zLabel,@yLabel)");
    scatterSeries->setMeshSmooth(true);
    scatterSeries->setBaseColor(Qt::yellow);
    scatter3D->addSeries(scatterSeries);

    scatter3D->axisX()->setTitle("axisX");
    scatter3D->axisX()->setLabelFormat("%.2f");
    scatter3D->axisX()->setTitleVisible(true);
    scatter3D->axisY()->setTitle("axisY");
    scatter3D->axisY()->setLabelFormat("%.2f");
    scatter3D->axisY()->setTitleVisible(true);
    scatter3D->axisZ()->setTitle("axisZ");
    scatter3D->axisZ()->setLabelFormat("%.2f");
    scatter3D->axisZ()->setTitleVisible(true);

    scatter3D->activeTheme()->setLabelBackgroundEnabled(false);
    scatterSeries->setMesh(QAbstract3DSeries::MeshSphere);
    scatterSeries->setItemSize(0.2);

    int N = 41;
    int itemCount = N * N;
    QScatterDataArray *dataArray = new QScatterDataArray;
    dataArray->resize(itemCount);
    QScatterDataItem *ptrToDataArray = &dataArray->first();
    float x,y,z;
    x = -10;
    for(int i = 1; i <= N ; i++)
    {
        y = -10;
        for(int j = 1; j <= N ;j++)
        {
            z = qSqrt(x*x + y*y);
            if(z != 0)
            {
                z = 10 * qSin(z)/z;
            }
            else
            {
                z = 10;
            }
            ptrToDataArray->setPosition(QVector3D(x,z,y));
            ptrToDataArray++;
            y += 0.5;
        }
        x +=0.5;
    }

    scatterSeries->dataProxy()->resetArray(dataArray);

}

void MainWindow::iniSurface3D()
{
    surface3D = new Q3DSurface();
    graphContainer = QWidget::createWindowContainer(surface3D,this);

    QValue3DAxis *axisX = new QValue3DAxis();
    axisX->setTitle("axisX");
    axisX->setTitleVisible(true);
    axisX->setLabelFormat("%.2f");
    //axisX->setRange(-11,11);
    axisX->setRange(-5000,5000);
    surface3D->setAxisX(axisX);

    QValue3DAxis *axisY = new QValue3DAxis();
    axisY->setTitle("axisY");
    axisY->setTitleVisible(true);
    axisY->setLabelFormat("%.2f");
    axisY->setAutoAdjustRange(true);
    //axisY->setRange(-11,11);
    surface3D->setAxisY(axisY);

    QValue3DAxis *axisZ = new QValue3DAxis();
    axisZ->setTitle("axisZ");
    axisZ->setTitleVisible(true);
    axisZ->setLabelFormat("%.2f");
    //axisZ->setRange(-11,11);
    axisZ->setRange(-5000,5000);
    surface3D->setAxisZ(axisX);

//    QSurfaceDataProxy *dataProxy = new QSurfaceDataProxy;
//    surfaceSeries = new QSurface3DSeries(dataProxy);
//    surfaceSeries->setItemLabelFormat("(@xLabel,@zLabel,@yLabel)");
//    surfaceSeries->setMeshSmooth(true);
//    surfaceSeries->setBaseColor(Qt::cyan);
//    surfaceSeries->setDrawMode(QSurface3DSeries::DrawSurfaceAndWireframe);
//    surfaceSeries->setFlatShadingEnabled(false);
//    surface3D->addSeries(surfaceSeries);

    QImage heightMapImage(":/mountain.png");
    QHeightMapSurfaceDataProxy *proxy = new QHeightMapSurfaceDataProxy(heightMapImage);
    proxy->setValueRanges(-5000,5000,-5000,5000);
    surfaceSeries = new QSurface3DSeries(proxy);
    surfaceSeries->setItemLabelFormat("(@xLabel,@zLabel,@yLabel)");
    surfaceSeries->setMeshSmooth(true);
    //surfaceSeries->setBaseColor(Qt::cyan);
    surfaceSeries->setDrawMode(QSurface3DSeries::DrawSurface);
    surfaceSeries->setFlatShadingEnabled(false);
    surface3D->addSeries(surfaceSeries);

//    int N = 41;
//    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
//    dataArray->reserve(N);
//    float x,y,z;
//    x = -10;
//    for(int i = 1; i <= N ; i++)
//    {
//        QSurfaceDataRow *dataRow = new QSurfaceDataRow(N);
//        y = -10;
//        int index = 0;
//        for(int j = 1; j <= N ;j++)
//        {
//            z = qSqrt(x*x + y*y);
//            if(z != 0)
//            {
//                z = 10 * qSin(z)/z;
//            }
//            else
//            {
//                z = 10;
//            }
//            (*dataRow)[index++].setPosition(QVector3D(x,z,y));
//            y += 0.5;
//        }
//        x +=0.5;
//        dataArray->append(dataRow);
//    }

//    dataProxy->resetArray(dataArray);
}


﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMainWindow>
#include <QtDataVisualization>
#include <Q3DBars>
#include <QBar3DSeries>
#include <QBarDataProxy>
#include <QBarDataArray>
#include <QBarDataItem>
#include <QBarDataRow>
#include <Q3DCamera>
#include <QSplitter>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

using namespace QtDataVisualization;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QtDataVisualization::Q3DBars *graph3D;
    QtDataVisualization::QBar3DSeries *series;
    QWidget *graphContainer;

    Q3DScatter *scatter3D;
    QScatter3DSeries *scatterSeries;

    Q3DSurface *surface3D;
    QSurface3DSeries *surfaceSeries;


    void iniGraph3D();

    void iniGraph3DP();

    void iniScatter3D();

    void iniSurface3D();

};
#endif // MAINWINDOW_H
